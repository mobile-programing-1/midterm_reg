import 'package:flutter/material.dart';

class mainPage extends StatelessWidget {
  const mainPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          const Center(
            child: Text(
              '* กำหนดการพิธีพระราชทานปริญญาบัตรปีการศึกษา 2563 และปีการศึกษา 2564',
              style: TextStyle(
                  fontSize: 23.0,
                  fontWeight: FontWeight.bold,
                  color: Colors.red),
            ),
          ),
          Image.network(
            "https://media.istockphoto.com/id/1310894617/vector/pozdravlenievupusknoy.jpg?s=612x612&w=0&k=20&c=ysYIN5Mr5IYCxFrxKV1jdOlQcGEuVb12qYr_sQlPxzY=",
            height: 200,
          ),
          ConstrainedBox(
            constraints: const BoxConstraints(maxWidth: 500.0),
            child: Text(
              _longText1,
              style: TextStyle(fontSize: 14.0, fontWeight: FontWeight.w600),
            ),
          ),
          ConstrainedBox(
            constraints: const BoxConstraints(maxWidth: 500.0),
            child: Text(
              _longText3,
              style: TextStyle(
                  fontSize: 23.0,
                  fontWeight: FontWeight.bold,
                  color: Colors.red),
            ),
          ),
          Image.network(
              "https://scontent.fbkk30-1.fna.fbcdn.net/v/t39.30808-6/325429589_2534626980009002_9025078851024509645_n.jpg?_nc_cat=106&ccb=1-7&_nc_sid=730e14&_nc_eui2=AeFZg43dUuK6maBfnedyCasa7DD0CgvAarzsMPQKC8BqvAZmMPS_1x1vaQDNCvE_QrKkcxhxFhP-03vl9Nnyh7Ww&_nc_ohc=IcWgzG-Px-gAX9cRXVU&_nc_ht=scontent.fbkk30-1.fna&oh=00_AfCIleHEwVmsuMAHYYrBkTnyOnHegW8Q65m2DOHI-_-FlQ&oe=63D71C9B"),
          ConstrainedBox(
            constraints: const BoxConstraints(maxWidth: 500.0),
            child: Text(
              _longText2,
              style: TextStyle(fontSize: 14.0, fontWeight: FontWeight.w600),
            ),
          ),
        ],
      ),
    );
  }
}

String _longText1 =
    '**ปีการศึกษา 2563 วันจันทร์ที่ 6 มีนาคม 2566 \n**ปีการศึกษา 2561 - 2562 วันอัีงคารที่ 7 มีนาคม 2566';

String _longText2 =
    '**ค่าไฟฟ้าห้องพักนิสิต กองกิจการนิสิต ภาคปลาย ปีการศึกษา 2565 \nประจำเดือน ธันวาคม พ.ศ. 2565 \n >> ตรวจสอบรายชื่อนิสิตค้างชำระค่าไฟฟ้า << \n- นิสิตสามารถตรวจสอบรายชื่อโดยแสกน qrcode ด้านบน หรือทาง facebook : สำนักงานหอพักนิสิต มหาวิทยาลัยบูรพา ';

String _longText3 = '* ประกาศจากสำนักงานหอพักนิสิต';
