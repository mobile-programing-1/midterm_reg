import 'dart:html';

import 'package:flutter/material.dart';

class LoginPage extends StatelessWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: ConstrainedBox(
        constraints: const BoxConstraints(maxWidth: 300.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            const Text(
              "เข้าสู่ระบบ",
              style: TextStyle(
                  fontSize: 24.0,
                  fontWeight: FontWeight.bold,
                  color: Color.fromARGB(255, 153, 165, 13)),
            ),
            Divider(),
            Image.network(
              "https://upload.wikimedia.org/wikipedia/commons/e/ec/Buu-logo11.png",
              height: 200,
            ),
            const Spacer(flex: 10),
            const TextField(decoration: InputDecoration(labelText: 'Username')),
            const Spacer(flex: 3),
            const TextField(decoration: InputDecoration(labelText: 'Password')),
            const Spacer(flex: 3),
            // const SizedBox(height: 30.0),
            ElevatedButton(onPressed: () {}, child: const Text('Login')),
            const Spacer(flex: 20),
          ],
        ),
      ),
    );
  }
}
