import 'package:flutter/material.dart';

class hsitoryStdPage extends StatelessWidget {
  const hsitoryStdPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          ConstrainedBox(
            constraints: const BoxConstraints(maxWidth: 500.0),
            child: Text(
              textAlign: TextAlign.center,
              head,
              style: TextStyle(
                  fontSize: 18.0,
                  fontWeight: FontWeight.bold,
                  color: Color.fromARGB(255, 0, 0, 0)),
            ),
          ),
          Divider(),
          Image.network(
            "https://scontent.fbkk30-1.fna.fbcdn.net/v/t39.30808-6/327907943_3334997700071814_9136886291815005370_n.jpg?_nc_cat=104&ccb=1-7&_nc_sid=5cd70e&_nc_ohc=GRv_ZUwRpUoAX_84wXx&tn=L3KcCZDCsfw4Fv7g&_nc_ht=scontent.fbkk30-1.fna&oh=00_AfD9SQPHBoaRV9oYG1_O4rdgUi0lWNyc_lB9flrvax-9Dw&oe=63DF3294",
            height: 200,
          ),
          Divider(),
          ConstrainedBox(
            constraints: const BoxConstraints(maxWidth: 500.0),
            child: Text(
              title,
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontSize: 16.0,
                  fontWeight: FontWeight.bold,
                  color: Colors.black),
            ),
          ),
          Divider(),
          ConstrainedBox(
            constraints: const BoxConstraints(maxWidth: 500.0),
            child: Text(
              body1,
              style: TextStyle(
                  fontSize: 12.0,
                  fontWeight: FontWeight.bold,
                  color: Colors.black),
            ),
          ),
          Divider(),
          ConstrainedBox(
            constraints: const BoxConstraints(maxWidth: 500.0),
            child: Text(
              textAlign: TextAlign.center,
              title2,
              style: TextStyle(
                  fontSize: 16.0,
                  fontWeight: FontWeight.bold,
                  color: Colors.black),
            ),
          ),
          Divider(),
          ConstrainedBox(
            constraints: const BoxConstraints(maxWidth: 500.0),
            child: Text(
              body2,
              style: TextStyle(
                  fontSize: 12.0,
                  fontWeight: FontWeight.bold,
                  color: Colors.black),
            ),
          ),
          Divider(),
        ],
      ),
    );
  }
}

String head = 'ประวัตินิสิต';
String title = 'ประวัติส่วนตัว';

String body1 =
    '\nรหัสนิสิต              63160223\nชื่อภาษาไทย         ศักดิธัช ทองเมือง\nชื่ออังกฤษ           MR. SUKDITUCH THONGMUANG\nสถานภาพ            กำลังศึกษา\nคณะ                    คณะวิทยาการสารสนเทศ\nหลักสูตร              วท.บ. (วิทยาการคอมพิวเตอร์) ปรับปรุง 59 - ป.ตรี 4 ปี ปกติ \nระดับการศึกษา    ปริญญาตรี\nวิธีรับเข้า              รับตรงทั่วประเทศ\nวุฒิก่อนเข้ารับการศึกษา              ม.6\nเกรดเฉลี่ยก่อนเข้ารับการศึกษา   3.2\nจบการศึกษาจาก                         สวนกุหลาบวิทยาลัย ธนบุรี\nอ.ที่ปรึกษา           อาจารย์ภูสิต กุลเกษม,ผู้ช่วยศาสตราจารย์ ดร.โกเมศ อัมพวัน';

String title2 = 'ผลการเรียน';
String body2 =
    'หน่วยกิตคำนวณ      94\nหน่วยกิตที่ผ่าน         94\nคะแนนเฉลี่ยสะสม     3.24';
