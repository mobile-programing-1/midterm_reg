import 'package:device_preview/device_preview.dart';
import 'package:flutter/material.dart';

import 'login_page.dart';
import 'NavBar.dart';
import 'mainpage.dart';
import 'registerpage.dart';
import 'learnTablePage.dart';
import 'historyStd.dart';

void main() {
  runApp(const ColumnExample());
}

class ColumnExample extends StatelessWidget {
  const ColumnExample({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DevicePreview(
      tools: const [
        DeviceSection(),
      ],
      builder: (context) => MaterialApp(
        debugShowCheckedModeBanner: false,
        useInheritedMediaQuery: true,
        builder: DevicePreview.appBuilder,
        locale: DevicePreview.locale(context),
        title: 'REG Burapha University',
        theme: ThemeData(
            // primarySwatch: Colors.grey,

            ),
        home: Scaffold(
          drawer: NavBar(),
          backgroundColor: Colors.white,
          appBar: AppBar(
            // title: const Text('REG Burapha University'),
            title: const Text('REG Burapha University'),
            backgroundColor: Colors.grey.shade800,
          ),
          body: const SafeArea(
            child: LoginPage(),
          ),
        ),
      ),
    );
  }
}
