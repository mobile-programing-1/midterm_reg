import 'package:flutter/material.dart';

class learnTablePage extends StatelessWidget {
  const learnTablePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          ConstrainedBox(
            constraints: const BoxConstraints(maxWidth: 500.0),
            child: Text(
              _longText4,
              style: TextStyle(
                  fontSize: 12.0,
                  fontWeight: FontWeight.bold,
                  color: Colors.black),
            ),
          ),
          Divider(),
          ConstrainedBox(
            constraints: const BoxConstraints(maxWidth: 500.0),
            child: Text(
              _longText1,
              style: TextStyle(
                  fontSize: 23.0,
                  fontWeight: FontWeight.bold,
                  color: Colors.red),
            ),
          ),
          Image.network(
            "https://scontent.fbkk30-1.fna.fbcdn.net/v/t39.30808-6/328326094_961568521496205_7588391070233189950_n.jpg?_nc_cat=104&ccb=1-7&_nc_sid=5cd70e&_nc_ohc=aV8w4iu8gpwAX8Dk0H6&_nc_ht=scontent.fbkk30-1.fna&oh=00_AfC-ZdHogkzqPAVGwHiM9NOhrf7o8BVgaEF7NROnn5oC5Q&oe=63DFDA9B",
            height: 200,
          ),
          ConstrainedBox(
            constraints: const BoxConstraints(maxWidth: 500.0),
            child: Text(
              _longText2,
              style: TextStyle(
                  fontSize: 12.0,
                  fontWeight: FontWeight.bold,
                  color: Colors.black),
            ),
          ),
          Divider(),
          ConstrainedBox(
            constraints: const BoxConstraints(maxWidth: 500.0),
            child: Text(
              _longText3,
              style: TextStyle(
                  fontSize: 23.0,
                  fontWeight: FontWeight.bold,
                  color: Colors.red),
            ),
          ),
          Image.network(
              "https://scontent.fbkk30-1.fna.fbcdn.net/v/t39.30808-6/328424756_568380058504500_2836465996226117355_n.jpg?_nc_cat=103&ccb=1-7&_nc_sid=5cd70e&_nc_ohc=VgXmHwdQ45QAX-7jNKL&_nc_ht=scontent.fbkk30-1.fna&oh=00_AfDxKU3LTKdQATeep-ksoRYzrHY91TeiKDZHZKaVVv6LCA&oe=63DE6CCA"),
        ],
      ),
    );
  }
}

String _longText1 = '* ตารางเรียน';
String _longText2 =
    '* ข้อมูลที่ปรากฎอยู่ในตารางเรียนประกอบด้วย รหัสวิชา (จำนวนหน่วยกิต) กลุ่ม, ห้องเรียนและอาคาร ตามลำดับ';
String _longText3 = '* ตารางสอบ';
String _longText4 =
    'ตารางเรียน/สอบของรายวิชาที่ลงทะเบียนไว้แล้ว\nชื่อ: ศักดิธัช ทองเมือง\nสถานภาพ: กำลังศึกษา\nคณะ: คณะวิทยาการสารสนเทศ\nหลักสูตร: วท.บ. (วิทยาการคอมพิวเตอร์) ปรับปรุง 59 - ป.ตรี 4 ปี ปกติ \nอ.ที่ปรึกษา: อาจารย์ภูสิต กุลเกษม,ผู้ช่วยศาสตราจารย์ ดร.โกเมศ อัมพวัน';
