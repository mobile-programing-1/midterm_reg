import 'package:flutter/material.dart';
import 'registerpage.dart';

class NavBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        // Remove padding
        padding: EdgeInsets.zero,
        children: [
          UserAccountsDrawerHeader(
            accountName: Text('63160223'),
            accountEmail: Text('63160223@go.buu.ac.th'),
            currentAccountPicture: CircleAvatar(
              child: ClipOval(
                child: Image.network(
                  'https://scontent.fbkk30-1.fna.fbcdn.net/v/t39.30808-6/275433601_2832462633720923_9087047462990892301_n.jpg?_nc_cat=100&ccb=1-7&_nc_sid=09cbfe&_nc_eui2=AeHkQLD9Q2ypij2W6XTDiKqJDjrNU5RNHpUOOs1TlE0elWYRvgSMQneCKEklz6O2XXCDH_2XPgfP4v3-YutIt2Ai&_nc_ohc=1p2Wof3OuzAAX8OgaeA&_nc_ht=scontent.fbkk30-1.fna&oh=00_AfCoqOALM67yquWTB7fWW-WqmSxfhW05RxcbPTjzyC8hvw&oe=63D81ADF',
                  fit: BoxFit.cover,
                  width: 90,
                  height: 90,
                ),
              ),
            ),
            decoration: BoxDecoration(
              color: Colors.grey.shade800,
              image: DecorationImage(
                  fit: BoxFit.fill,
                  image: NetworkImage(
                      'https://live.staticflickr.com/1355/793041033_0d272583df_b.jpg')),
            ),
          ),
          ListTile(
            leading: Icon(Icons.home),
            title: Text('เมนูหลัก'),
            onTap: () => null,
          ),
          ListTile(
            leading: Icon(Icons.notifications),
            title: Text('ลงทะเบียน'),
            onTap: () => null,
          ),
          ListTile(
            leading: Icon(Icons.calendar_today),
            title: Text('ตารางเรียน'),
            onTap: () => null,
          ),
          ListTile(
            leading: Icon(Icons.face),
            title: Text('ประวัตินิสิต'),
            onTap: () => null,
          ),
          Divider(),
          ListTile(
            leading: Icon(Icons.monetization_on),
            title: Text('ค่าใช้จ่าย'),
            onTap: () => null,
          ),
          ListTile(
            leading: Icon(Icons.description),
            title: Text('ยื่นคำร้อง'),
            onTap: () => null,
          ),
          Divider(),
          ListTile(
            title: Text('เกี่ยวกับ'),
            leading: Icon(Icons.settings),
            onTap: () => null,
          ),
          ListTile(
            title: Text('ออกจากระบบ'),
            leading: Icon(Icons.exit_to_app),
            onTap: () => null,
          ),
        ],
      ),
    );
  }

  _onLocationTap(BuildContext context, int locationID) {
    // TODO later in this lesson, navigation!
    print('do something');
  }
}
